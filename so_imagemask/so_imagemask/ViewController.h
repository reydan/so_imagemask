//
//  ViewController.h
//  so_imagemask
//
//  Created by Andrei Stanescu on 7/18/13.
//  Copyright (c) 2013 Andrei Stanescu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
}
@property (weak, nonatomic) IBOutlet UIView *mainContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;
@property (weak, nonatomic) IBOutlet UIImageView *destImageView;
- (IBAction)onMask:(id)sender;
- (IBAction)onCopyImage:(id)sender;

@end
