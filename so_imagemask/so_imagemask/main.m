//
//  main.m
//  so_imagemask
//
//  Created by Andrei Stanescu on 7/18/13.
//  Copyright (c) 2013 Andrei Stanescu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
