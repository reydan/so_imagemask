//
//  ViewController.m
//  so_imagemask
//
//  Created by Andrei Stanescu on 7/18/13.
//  Copyright (c) 2013 Andrei Stanescu. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




// Here you control what solution you desire. Please do not put them both on 1.
#define USE_CHOICE1  1
#define USE_CHOICE2  0


- (IBAction)onMask:(id)sender {
    
#if USE_CHOICE1
    UIImage* maskImage = [UIImage imageNamed:@"star_white.png"];
#endif
    
#if USE_CHOICE2
    UIImage* maskImage = [UIImage imageNamed:@"star.png"];
#endif
    
    UIImageView* maskImageView = [[UIImageView alloc] initWithImage:maskImage];
    maskImageView.contentMode = UIViewContentModeScaleAspectFit;
    maskImageView.frame = _mainContainerView.bounds;
    
    _mainContainerView.layer.mask = maskImageView.layer;
    
    
}

- (IBAction)onCopyImage:(id)sender {
    
    _destImageView.image = [self getImageFromUIView:_mainContainerView];
}


- (UIImage*)getImageFromUIView:(UIView*)srcview
{
    // Get the image from the mainImageView
    UIGraphicsBeginImageContextWithOptions(srcview.bounds.size, FALSE, [[UIScreen mainScreen] scale]);
    [srcview.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if (srcview.layer.mask == nil)
        return img;
    
    
    // Enable this if using IOS6

        
    float retinaScale = [[UIScreen mainScreen] scale];
    
    ///////////////////////////////////////////////////////////////////////////////////////
    // PLEASE USE ONE OF THE BLOCKS BELOW. I EXPLAINED THE PROs AND CONs OF EACH CHOICE.
    ///////////////////////////////////////////////////////////////////////////////////////
        
#if USE_CHOICE1
    // CHOICE 1: Using a grayscale context as a mask
    // This is a solution a bit more optimized. You create only one context and then an image for it.
    // Please note that using this version, means you need to have the masks where WHITE = good, BLACK = will be removed.
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    CGContextRef ctx = CGBitmapContextCreate(nil, srcview.bounds.size.width * retinaScale, srcview.bounds.size.height * retinaScale, 8, 0, colorSpace, kCGImageAlphaNone);
    CGContextScaleCTM(ctx, retinaScale, retinaScale);
    CGContextScaleCTM(ctx, 1.0f, -1.0f);
    CGContextTranslateCTM(ctx, 0.0f, -srcview.bounds.size.height);
    [srcview.layer.mask renderInContext:ctx];
    CGImageRef mask = CGBitmapContextCreateImage(ctx);
    CGContextRelease(ctx);
#endif
        
#if USE_CHOICE2
    // CHOICE 2: using a dedicated image mask
    // This solution uses a bit more memory. It creates a context, and image and then a image mask.
    // This solution uses the mask where BLACK = good, WHITE = will be removed.
    
    UIGraphicsBeginImageContextWithOptions(srcview.bounds.size, TRUE, retinaScale);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(ctx, [UIColor whiteColor].CGColor);
    CGContextFillRect(ctx, _mainContainerView.bounds);
    
    [srcview.layer.mask renderInContext:ctx];
    UIImage * maskimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    // Create a image mask from the UIImage
    CGImageRef maskRef = maskimg.CGImage;
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
#endif
        
    // Apply the mask to our source image
    CGImageRef maskedimg= CGImageCreateWithMask(img.CGImage, mask);
    
    // Convert to UIImage so we can easily display it in a UIImageView
    img = [UIImage imageWithCGImage:maskedimg scale:img.scale orientation:img.imageOrientation];
    
    CGImageRelease(mask);
    CGImageRelease(maskedimg);

    return img;
    
}
@end
